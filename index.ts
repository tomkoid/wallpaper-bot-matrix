import {
  MatrixClient,
  SimpleFsStorageProvider,
  AutojoinRoomsMixin,
  MatrixEvent,
} from "matrix-bot-sdk";

import { getWallpaper } from "./events/wallpaper";

import * as dotenv from "dotenv";
dotenv.config();

const homeserverUrl = process.env.HOMESERVER;
const accessToken = process.env.ACCESS_TOKEN;
const storage = new SimpleFsStorageProvider("bot.json");

if (!homeserverUrl || !accessToken) {
  console.log("HOMESERVER or ACCESS_TOKEN not defined in .env");
  process.exit(1);
}

export const client = new MatrixClient(homeserverUrl, accessToken, storage);
AutojoinRoomsMixin.setupOnClient(client)

client.on("room.message", async (roomId: string, event: MatrixEvent) => {
  if (event["content"]?.["msgtype"] !== "m.text") return;
  if (event["sender"] === await client.getUserId()) return;

  const body: string = event["content"]["body"];
  
  if (body.startsWith("!wallpaper")) {
    return await getWallpaper(roomId, event)
  }
})

client.start().then(() => {
  console.log("Bot started")
})
