import { MatrixEvent } from "matrix-bot-sdk";
import axios from "axios";
import { client } from "..";

export async function getWallpaper(roomId: string, event: MatrixEvent) {
  try {
    const parameters = new URLSearchParams({
      sorting: "random",
    })

    await client.setTyping(roomId, true);

    const response = await fetch(`https://wallhaven.cc/api/v1/search?${parameters}`);
    const data = await response.json();

    const wallpaper = data.data[0];
    const responseImage = await axios.get(wallpaper.path, { responseType: "arraybuffer" });
    const responseImageType: string = responseImage.headers['content-type'];
    const contentUploadResponseUrl = await client.uploadContent(await responseImage.data, responseImageType)
    await client.setTyping(roomId, false);

    const replyMessage = `👁️ Views: ${wallpaper.views}\n⭐ Favorites: ${wallpaper.favorites}\n🍎 Category: ${wallpaper.category}\n🖼️ Resolution: ${wallpaper.resolution}\n🔗 Wallpaper on Wallhaven: ${wallpaper.short_url}`
    await client.replyHtmlText(roomId, event, replyMessage)
    await client.sendMessage(roomId, {
      msgtype: "m.image",
      body: wallpaper.id,
      url: contentUploadResponseUrl
    })
  } catch (err) {
    try {
      await client.replyNotice(roomId, event, `❌ Something went wrong: ${err}`);
    } catch(err) {
      console.error(err)
    }

    return;
  }
  // await client.sendMe
} 
